package com.be1eye.cco.blogpluginpart1;

import com.sap.scco.ap.plugin.BasePlugin;

/**
 * Hello world!
 *
 */
public class App extends BasePlugin
{

	@Override
	public String getId() {

		return "blogpluginpartone";
	}

	@Override
	public String getName() {
		
		return "CCO Plugin Part 1";
	}

	@Override
	public String getVersion() {

		return getClass().getPackage().getImplementationVersion();
	}
	
}
